import sqlite3
import json
import re
import mysql.connector


# find Region and Check Province
def find_Province(province : str):
    f = open('file.json',encoding='UTF-8')
    data = json.loads(f.read())

    for item in data['ProvinceandRegionComprehensive']:
       splitProvince =  re.split(r"จังหวัด",item['ProvinceNameThai'])
       Province = re.search(splitProvince[1],province)
       if(Province):
          return Province[0],item['RegionName']
       
# find District
def find_District(District: str):
   DstProvince = re.search(r'[อ][ำ][ก-๙]*',District)
   if(DstProvince != None):
      DstProvince = re.split(r"อำเภอ",DstProvince[0])
      
      return DstProvince[1]
   else:
      DstProvince = re.findall(r'[อ]\.[ก-๙]*',District)
      DstProvince = re.split(r"อ.",DstProvince[0])
      
      return DstProvince[1]

# con1 = sqlite3.connect("BIDW-DB.db") # <= Original DB
# con2 = sqlite3.connect("DW DATA BASE")# <= DW DB

mydb = mysql.connector.connect(
  host="angsila.informatics.buu.ac.th",
  user="guest05",
  password="CvDZm5Pf",
  database="guest05"
)
 # # Connect DB and Query Stage
# QueryDB1 = con1.cursor()
# QueryDB2 = con2.cursor()
# res_Store_Address = mycursor.execute('SELECT store_name,store_address FROM Store')
# DB_Store_address = res_Store_Address.fetchall()

mycursor = mydb.cursor()
mycursor.execute('SELECT name,address FROM store')
DB_Store_address = mycursor.fetchall()

count =0
for item in DB_Store_address:
   
   # Store name
   Store_name = item[0]
   # find zipcode 
   zipcode =  re.findall(r"[0-9][0-9][0-9][0-9]", str(item[1]))

   #find Province
   strProvince = re.findall(r'[จ].[ก-๙]*',str(item[1]))
   #find DstProvince
   DstProvince = find_District(str(item[1]))
   Province, Region =(find_Province(strProvince[0]))  # type: ignore
   
   # QueryDB2.execute("INSERT INTO Store_DW (Store_name,Store_City,Store_Region,Store_District) VALUES(?,?,?,?)",(Store_name,Province,Region,DstProvince)) 
   # con2.commit()

   #soure output
   print(f'Raw Data :{item}')
   print(Store_name)
   print(DstProvince)
   print(Province)
   print(Region)

   count = count + 1
   print('#######################################')
print(f'record {count}')



  
